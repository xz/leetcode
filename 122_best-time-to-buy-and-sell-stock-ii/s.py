class Solution:

    def maxProfit(self, prices: 'list[int]') -> 'int':
        return sum([x - y for x, y in zip(prices[1:], prices[:-1]) if x > y])


s = Solution()
print(s.maxProfit([7, 1, 5, 3, 6, 4]))
print(s.maxProfit([1, 2, 3, 4, 5]))
print(s.maxProfit([7, 6, 4, 3, 1]))
print(s.maxProfit([7, 6]))
print(s.maxProfit([6, 7]))
print(s.maxProfit([3]))
print(s.maxProfit([]))
