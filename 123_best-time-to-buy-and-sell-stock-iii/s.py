class Solution:

    def maxProfit(self, prices: 'list[int]') -> 'int':
        B1, S2, B2, S3 = 0, 0, 0, 0
        for i in range(1, len(prices)):
            pd = prices[i] - prices[i - 1]
            B1, S2, B2, S3 = max(0, B1) + pd, max(B1, S2), max(S2, B2) + pd, max(B2, S3)

        return max(0, B1, S2, B2, S3)


s = Solution()
print(s.maxProfit([3, 3, 5, 0, 0, 3, 1, 4]))
print(s.maxProfit([1, 2, 3, 4, 5]))
print(s.maxProfit([7, 6, 4, 3, 1]))
