def find_rook(board):
    for i in range(8):
        for j in range(8):
            if board[i][j] == 'R':
                return (i, j)


def search(board, ri, rj, di, dj):
    i = ri
    j = rj
    while True:
        i += di
        if not (0 <= i < 8):
            return 0
        j += dj
        if not (0 <= j < 8):
            return 0
        if board[i][j] == 'p':
            return 1
        if board[i][j] == 'B':
            return 0


class Solution(object):

    def numRookCaptures(self, board):
        """
        :type board: List[List[str]]
        :rtype: int
        """
        ri, rj = find_rook(board)
        total = 0
        for di, dj in [[0, 1], [1, 0], [0, -1], [-1, 0]]:
            total += search(board, ri, rj, di, dj)

        return total


s = Solution()
print(
    s.numRookCaptures(
        [
            [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "p", ".", ".", ".", "."],
            [".", ".", ".", "R", ".", ".", ".", "p"], [".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "p", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", ".", ".", ".", ".", "."]
        ]
    )
)

print(
    s.numRookCaptures(
        [
            [".", ".", ".", ".", ".", ".", ".", "."], [".", "p", "p", "p", "p", "p", ".", "."],
            [".", "p", "p", "B", "p", "p", ".", "."], [".", "p", "B", "R", "B", "p", ".", "."],
            [".", "p", "p", "B", "p", "p", ".", "."], [".", "p", "p", "p", "p", "p", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", ".", ".", ".", ".", "."]
        ]
    )
)
print(
    s.numRookCaptures(
        [
            [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "p", ".", ".", ".", "."],
            [".", ".", ".", "p", ".", ".", ".", "."], ["p", "p", ".", "R", ".", "p", "B", "."],
            [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "B", ".", ".", ".", "."],
            [".", ".", ".", "p", ".", ".", ".", "."], [".", ".", ".", ".", ".", ".", ".", "."]
        ]
    )
)
