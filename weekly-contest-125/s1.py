from collections import defaultdict


class Solution:

    def findJudge(self, N, trust):
        trusters = defaultdict(list)
        trustees = defaultdict(list)
        for t in trust:
            trustees[t[0]].append(t[1])
            trusters[t[1]].append(t[0])

        for i in range(1, N + 1):
            if len(trustees[i]) == 0 and len(trusters[i]) == N - 1:
                return i

        return -1


s = Solution()
print(s.findJudge(2, [[1, 2]]))
print(s.findJudge(3, [[1, 3], [2, 3]]))
print(s.findJudge(3, [[1, 3], [2, 3], [3, 1]]))
