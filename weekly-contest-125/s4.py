from collections import defaultdict


class Solution:

    def gridIllumination(self, N, lamps, queries):
        row = defaultdict(set)
        col = defaultdict(set)
        diag = defaultdict(set)
        adiag = defaultdict(set)

        for i, j in lamps:
            row[i].add((i, j))
            col[j].add((i, j))
            diag[j - i].add((i, j))
            adiag[j + i].add((i, j))

        print(row)
        print(col)
        print(diag)
        print(adiag)

        results = []
        removed = set()
        for i, j in queries:
            print()
            print('----')
            print((i, j))

            print(
                (
                    len(row[i].difference(removed)), len(col[j].difference(removed)),
                    len(diag[j - i].difference(removed)), len(adiag[j + i].difference(removed))
                )
            )
            if len(row[i].difference(removed)) > 0 or len(col[j].difference(removed)) > 0 or len(
                diag[j - i].difference(removed)
            ) > 0 or len(adiag[j + i].difference(removed)) > 0:
                results.append(1)
            else:
                results.append(0)

            removed = removed.union([(ii, jj) for ii, jj in row[i] if abs(jj - j) <= 1])
            removed = removed.union([(ii, jj) for ii, jj in col[j] if abs(ii - i) <= 1])
            removed = removed.union([(ii, jj) for ii, jj in diag[j - i] if abs((jj + ii) - (j + i)) <= 1])
            removed = removed.union([(ii, jj) for ii, jj in adiag[j + i] if abs((jj - ii) - (j - i)) <= 1])

            print(row)
            print(col)
            print(diag)
            print(adiag)
            print(removed)

        return results


s = Solution()
print(s.gridIllumination(5, [[0, 0], [4, 4]], [[1, 1], [1, 0]]))
