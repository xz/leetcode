# from lc import TreeNode, make_bintree, print_bintree


class Solution(object):

    def insertIntoMaxTree(self, root, val):
        """
        :type root: TreeNode
        :type val: int
        :rtype: TreeNode
        """
        # get A
        A = []

        def dfs(x):
            nonlocal A
            if x is None:
                return

            dfs(x.left)
            A.append(x.val)
            dfs(x.right)

        dfs(root)

        A += [val]

        # print(A)

        def imax(seq):
            return max(range(len(seq)), key=seq.__getitem__)

        def build_tree(i, j):
            if i == j:
                return None
            gi = imax(A[i:j]) + i
            x = TreeNode(A[gi])
            x.left = build_tree(i, gi)
            x.right = build_tree((gi + 1), j)
            return x

        return build_tree(0, len(A))


# s = Solution()
# print_bintree(s.insertIntoMaxTree(make_bintree([4, 1, 3, None, None, 2]), 5))
# print_bintree(s.insertIntoMaxTree(make_bintree([5, 2, 4, None, 1]), 3))
# print_bintree(s.insertIntoMaxTree(make_bintree([5, 2, 3, None, 1]), 4))
