from typing import List


class Solution:

    def asteroidCollision(self, asteroids: List[int]) -> List[int]:
        left = []
        right = []
        for ast in asteroids:
            if ast > 0:
                left.append(ast)
            else:
                while left:
                    if -ast > left[-1]:
                        left.pop()
                        continue

                    if -ast == left[-1]:
                        left.pop()
                        break

                    # if -ast < left[-1]:
                    break
                else:
                    right.append(ast)

        return right + left


s = Solution()
print(s.asteroidCollision([5, 10, -5]))
print(s.asteroidCollision([8, -8]))
print(s.asteroidCollision([10, 2, -5]))
print(s.asteroidCollision([-2, -1, 1, 2]))
