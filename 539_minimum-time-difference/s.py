class Solution:

    def findMinDifference(self, timePoints: 'list[str]') -> 'int':
        # mfz = minutes from zero
        mfzs = sorted([int(tp[:2]) * 60 + int(tp[3:]) for tp in timePoints])
        mfzs += [mfzs[0] + 24 * 60]
        return min(y - x for x, y in zip(mfzs, mfzs[1:]))


s = Solution()
print(s.findMinDifference(["23:59", "00:00"]) == 1)
print(s.findMinDifference(["23:59", "00:01"]) == 2)
print(s.findMinDifference(['23:58', "23:59", "00:01"]) == 1)
print(s.findMinDifference(['00:01', '23:55', "23:58", "00:00"]) == 1)
