from typing import List


class Solution:

    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        id_ = 0
        lookup = {0: {}}
        for word in wordDict:
            p = 0
            for letter in word:
                if letter not in lookup[p]:
                    id_ += 1
                    lookup[p][letter] = id_
                    lookup[id_] = {}

                p = lookup[p][letter]

            lookup[p]['.'] = None

        for k, v in lookup.items():
            print(f"{k}: {v}")

        last_state = set([0])
        i = 0
        n = len(s)
        while i < n:
            new_state = set()
            x = s[i]
            for p in last_state:
                if x in lookup[p]:
                    new_state.add(lookup[p][x])

                if '.' in lookup[p] and x in lookup[0]:
                    new_state.add(lookup[0][x])

            if len(new_state) == 0:
                break

            last_state = new_state
            i += 1

        if not (i == n and any('.' in lookup[p] for p in last_state)):
            return []

        last_state = [(0, '')]
        i = 0
        n = len(s)
        while i < n:
            # print(last_state)
            new_state = []
            x = s[i]
            for p, lst in last_state:
                if x in lookup[p]:
                    new_state.append((lookup[p][x], lst + x))

                if '.' in lookup[p] and x in lookup[0]:
                    new_state.append((lookup[0][x], lst + ' ' + x))

            if len(new_state) == 0:
                break

            last_state = new_state
            i += 1

        if i == n:
            return sorted([str_ for p, str_ in last_state if '.' in lookup[p]])
        else:
            return []


s = Solution()
print(s.wordBreak("leetcode", ['ab', 'aba', 'b', 'ca', 'cc']))
# print(s.wordBreak("leetcode", ["leet", "code"]))
# print(s.wordBreak("catsanddog", ["cat", "cats", "and", "sand", "dog"]))
# print(s.wordBreak("pineapplepenapple", ["apple", "pen", "applepen", "pine", "pineapple"]))
# print(s.wordBreak("catsandog", ["cats", "dog", "sand", "and", "cat"]))
# print(s.wordBreak("aaaaaaaa", ["a", "aa", "aaa"]))
# print(
#     s.wordBreak(
#         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
#         ["a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa", "aaaaaaaa", "aaaaaaaaa", "aaaaaaaaaa"]
#     )
# )
