from collections import Counter


def solve_with_dp(k, prices):
    S, B = [0 for _ in range(k + 1)], [0 for _ in range(k)]
    for i in range(1, len(prices)):
        pd = prices[i] - prices[i - 1]
        S, B = [0] + [max(s, b) for s, b in zip(S[1:], B)], [max(b, s) + pd for b, s in zip(B, S[:-1])]

    return max(S + B)


def sum_positive_diffs(prices):
    return sum([x - y for x, y in zip(prices[1:], prices[:-1]) if x > y])


class Solution:

    def maxProfit(self, k: 'int', prices: 'list[int]') -> 'int':
        if k >= len(prices) / 2:
            return sum_positive_diffs(prices)
        else:
            return solve_with_dp(k, prices)


s = Solution()
print(s.maxProfit(2, [3, 2, 6, 5, 0, 3, 1, 2, 1, 2, 1, 2] * 100))
print(s.maxProfit(1000000000, [3, 2]))  # 0
print(s.maxProfit(2, [1, 1]))  # 0
print(s.maxProfit(2, [1]))  # 0
print(s.maxProfit(2, []))  # 0
