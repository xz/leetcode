class Solution:

    def integerBreak(self, n: 'int') -> 'int':
        if n == 2:
            return 1

        if n == 3:
            return 2

        if n == 4:
            return 4

        return 3**((n - 2) // 3) * ((n - 2) % 3 + 2)


s = Solution()
print(s.integerBreak(2))
print(s.integerBreak(10))
print(s.integerBreak(11))
print(s.integerBreak(12))
