from typing import List


class Solution:

    def numIslands(self, grid: List[List[str]]) -> int:
        if len(grid) == 0:
            return 0

        n = len(grid)
        m = len(grid[0])

        def dfs(i, j):
            if i < 0 or i >= n:
                return 0

            if j < 0 or j >= m:
                return 0

            if grid[i][j] == '0':
                return 0

            grid[i][j] = '0'

            return 1 + sum([dfs(i + di, j + dj) for di, dj in [(0, 1), (0, -1), (1, 0), (-1, 0)]])

        n_islands = 0
        for i in range(n):
            for j in range(m):
                n_islands += 1 if dfs(i, j) > 0 else 0

        return n_islands


s = Solution()
print(
    s.numIslands(
        [["1", "1", "1", "1", "0"],\
         ["1", "1", "0", "1", "0"],\
         ["1", "1", "0", "0", "0"],\
         ["0", "0", "0", "0", "0"]]
    )
)
print(
    s.numIslands(
        [["1", "1", "1", "0", "0"],\
         ["1", "1", "0", "0", "0"],\
         ["0", "0", "1", "0", "0"],\
         ["0", "0", "0", "1", "1"]]
    )
)
