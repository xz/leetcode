from lc import make_bintree, traverse_bintree, TreeNode


class Solution:

    def longestConsecutive(self, root: 'TreeNode') -> 'int':

        if root is None:
            return 0

        best = 1

        def dfs(x, y, l):
            nonlocal best

            if x is None:
                return

            if y is not None and x.val - y.val == 1:
                ll = l + 1
                best = max(ll, best)
            else:
                ll = 1

            dfs(x.left, x, ll)
            dfs(x.right, x, ll)

        dfs(root, None, None)

        return best


s = Solution()

xs1 = [1, None, 3, 2, 4, None, None, None, 5]
print(xs1)
t1 = make_bintree(xs1)
print(traverse_bintree(t1))
print(s.longestConsecutive(t1))

xs1 = [2, None, 3, 2, None, 1, None]
print(xs1)
t1 = make_bintree(xs1)
print(traverse_bintree(t1))
print(s.longestConsecutive(t1))
