# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import defaultdict


def dfs(root, i, j, ss):
    if root is None:
        return

    ss[j].add(root.val)
    dfs(root.left, i + 1, j - 1, ss)
    dfs(root.right, i + 1, j + 1, ss)


class Solution:

    def verticalTraversal(self, root):
        ss = defaultdict(set)
        dfs(root, 0, 0, ss)
        return [sorted(ss[k]) for k in sorted(ss.keys())]
