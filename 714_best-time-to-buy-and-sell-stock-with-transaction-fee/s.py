import math


class Solution:

    def maxProfit(self, prices: 'list[int]', fee: 'int') -> 'int':
        S, B = 0, -math.inf
        for i in range(1, len(prices)):
            pd = prices[i] - prices[i - 1]
            S, B = max(S, B), max(S - fee, B) + pd

        return max(S, B)


s = Solution()
print(s.maxProfit([1, 3, 2, 8, 4, 9], 2))
print(s.maxProfit([1, 3], 2))
print(s.maxProfit([1], 2))
print(s.maxProfit([], 2))
