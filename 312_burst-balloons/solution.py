from functools import lru_cache


class Solution:

    def maxCoins(self, nums):
        nums = [1] + nums + [1]

        @lru_cache(None)
        def dfs(l, r):
            if l == r:
                return 0

            return max(dfs(l, i) + nums[l - 1] * nums[i] * nums[r] + dfs(i + 1, r) for i in range(l, r))

        return dfs(1, len(nums) - 1)


s = Solution()
print(s.maxCoins([]))
print(s.maxCoins([8, 2, 6, 8, 9, 8, 1, 4, 1, 5, 3, 0, 7, 7, 0, 4, 2, 2, 5]))
print(s.maxCoins([1, 2, 1000, 2, 1]))
print(s.maxCoins([3, 1, 5, 8]))
