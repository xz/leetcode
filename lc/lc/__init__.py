from collections import deque


# Definition for a binary tree node.
class TreeNode:

    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def make_bintree(xs):
    xs = deque(xs)
    x = xs.popleft()

    if x is None:
        return None

    root = TreeNode(x)
    rs = deque([root])

    while rs:
        r = rs.popleft()

        x = xs.popleft() if xs else None
        if x is None:
            r.left = None
        else:
            t = TreeNode(x)
            r.left = t
            rs.append(t)

        x = xs.popleft() if xs else None
        if x is None:
            r.right = None
        else:
            t = TreeNode(x)
            r.right = t
            rs.append(t)

    return root


def print_bintree(x, prefix=''):
    if x is None:
        print(prefix + f".")
        return

    print(prefix + f"{x.val}")
    print_bintree(x.left, prefix + '  ')
    print_bintree(x.right, prefix + '  ')
