"""
Kadane's algorithm
"""


class Solution:

    def maxProfit(self, prices: 'list[int]') -> 'int':
        ds = [x - y for x, y in zip(prices[1:], prices[:-1])]
        accs = [0]
        for d in ds:
            accs.append(max(accs[-1] + d, 0))
        return max(accs)


s = Solution()
print(s.maxProfit([7, 1, 5, 3, 6, 4]))
print(s.maxProfit([7, 6, 4, 3, 1]))
print(s.maxProfit([]))
print(s.maxProfit([7]))
print(s.maxProfit([7, 1]))
print(s.maxProfit([1, 7]))
print(s.maxProfit([1, 7, 1]))
