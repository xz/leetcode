# Definition for singly-linked list with a random pointer.
class RandomListNode(object):

    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None


class Solution(object):

    def copyRandomList(self, head):
        if head is None:
            return None

        p = head

        lookup = {None: None}
        while p is not None:
            link = RandomListNode(p.label)
            link.next = None if p.next is None else p.next.label
            link.random = None if p.random is None else p.random.label
            lookup[p.label] = link
            p = p.next

        for label, link in lookup.items():
            link.next = lookup[link.next]
            link.random = lookup[link.random]

        return lookup[p.label]
