from collections import Counter


def dfs_perms(letter_resource, prefix, callback):
    if not bool(letter_resource):
        # if letter_resource is empty, we have depleted all letters
        callback(prefix)
        return

    remaining_letters = letter_resource.keys()
    for letter in remaining_letters:
        new_letter_dict = letter_resource.copy()
        new_letter_dict[letter] -= 1
        if new_letter_dict[letter] == 0:
            del new_letter_dict[letter]
        dfs_perms(new_letter_dict, prefix + letter, callback)


class Solution:

    def add_results(self, value):
        result = value + self.core_letter + value[::-1]
        self.results.append(result)

    def generatePalindromes(self, s):
        letter_counter = Counter(s)
        letter_resource = {}
        self.core_letter = ''
        for letter, count in letter_counter.items():
            if count % 2 != 0:
                if self.core_letter != '':
                    # there are more than one letters with odd number of occurence.
                    # this means that no palidromes can be generated
                    return []

                self.core_letter = letter
                if count > 1:
                    letter_resource[letter] = (count - 1) // 2
            else:
                letter_resource[letter] = count // 2

        self.results = []
        dfs_perms(letter_resource, '', self.add_results)
        return sorted(self.results)


s = Solution()
print(s.generatePalindromes(''))
print(s.generatePalindromes('a'))
print(s.generatePalindromes('abc'))
print(s.generatePalindromes('aabb'))
print(s.generatePalindromes('aaabccb'))
