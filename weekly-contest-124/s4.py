import math
from collections import Counter


def is_square(x):
    y = math.sqrt(x)
    return y == math.floor(y)


class Solution:

    def numSquarefulPerms(self, A):
        n = len(A)
        counter = Counter(A)
        n_perms = 0

        cands = {x: {y for y in counter if is_square(x + y)} for x in counter}
        cands[None] = set(counter)

        def dfs(x, n_letters):
            nonlocal n_perms
            if n_letters == n:
                n_perms += 1
                return

            for c in cands[x]:
                if counter[c]:
                    counter[c] -= 1
                    dfs(c, n_letters + 1)
                    counter[c] += 1

        dfs(None, 0)

        return n_perms


s = Solution()
print(s.numSquarefulPerms([1, 17, 8]))
print(s.numSquarefulPerms([99, 62, 10, 47, 53, 9, 83, 33, 15, 24]))
