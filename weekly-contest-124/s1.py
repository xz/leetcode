# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution:

    def isCousins(self, root: 'TreeNode', x: 'int', y: 'int') -> 'bool':

        val_to_node = {}

        def dfs(x, depth, parent):
            if x is None:
                return

            nonlocal val_to_node
            val_to_node[x.val] = (parent, depth)
            dfs(x.left, depth + 1, x)
            dfs(x.right, depth + 1, x)

        dfs(root, 0, None)

        xn = val_to_node[x]
        yn = val_to_node[y]

        return xn[1] == yn[1] and xn[0] != yn[0]
