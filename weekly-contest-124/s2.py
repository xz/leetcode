from collections import deque, defaultdict

directions = [
    (-1, 0),
    (1, 0),
    (0, -1),
    (0, 1),
]


class Solution:

    def orangesRotting(self, grid: 'list[list[int]]') -> 'int':
        n = len(grid)
        m = len(grid[0])

        newly_rotten = []
        n_oranges = 0
        for i in range(n):
            for j in range(m):
                if grid[i][j] > 0:
                    n_oranges += 1

                if grid[i][j] == 2:
                    newly_rotten.append((i, j))

        if n_oranges == 0:
            return 0

        n_rotten = 0
        n_mins = -1
        while newly_rotten:
            n_mins += 1
            n_rotten += len(newly_rotten)
            last_rotten = newly_rotten
            newly_rotten = []
            for i, j in last_rotten:
                for di, dj in directions:
                    ii, jj = i + di, j + dj
                    if 0 <= ii < n and 0 <= jj < m and grid[ii][jj] == 1:
                        newly_rotten.append((ii, jj))
                        grid[ii][jj] = 2

        if n_rotten == n_oranges:
            return n_mins
        else:
            return -1


s = Solution()
print(s.orangesRotting([[2, 1, 1], [1, 1, 0], [0, 1, 1]]))
print(s.orangesRotting([[2, 1, 1], [0, 1, 1], [1, 0, 1]]))
print(s.orangesRotting([[1], [2]]))
