from collections import deque


class Solution:

    def minKBitFlips(self, A: 'list[int]', K: 'int') -> 'int':
        n_flips = 0
        q = deque()
        even_flips = True
        for i in range(len(A)):
            if len(q) > 0 and i == q[0]:
                q.popleft()
                even_flips = not even_flips

            if (A[i] == 0) == even_flips:
                if i > len(A) - K:
                    return -1

                n_flips += 1
                q.append(i + K)
                even_flips = not even_flips

        return n_flips


s = Solution()
print(s.minKBitFlips([0, 1, 0], 1))
print(s.minKBitFlips([1, 1, 0], 2))
print(s.minKBitFlips([0, 0, 0, 1, 0, 1, 1, 0], 3))
print(s.minKBitFlips([1], 1))
print(s.minKBitFlips([1, 1, 1, 1], 3))
print(s.minKBitFlips([0, 1, 1, 0], 3))
print(s.minKBitFlips([0, 0, 0, 0], 3))
