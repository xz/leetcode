"""
Convert the problem into the diff space. We are asked to collect the largest
price diffs, with the cooldown constraint.

In the diff space, there are n-1 steps, each takes one of three states:

    B  = collecting the current price diff
    S0 = not collecting, but on cooldown
    S1 = not collecting, and can buy

Now we've converted the problem into a state transition graph:

    B  -->  B or S0
    S0 -->  S1
    S1 -->  S1 or B

Note that every time we arrive at state B, we gain score (prices[i] - prices[i-1]).
"""


class Solution:

    def maxProfit(self, prices: 'list[int]') -> 'int':
        B, S0, S1 = 0, 0, 0
        for i in range(1, len(prices)):
            pd = prices[i] - prices[i - 1]
            B, S0, S1 = max(B, S1) + pd, B, max(S0, S1)

        return max(B, S0, S1)


s = Solution()
print(s.maxProfit([1, 2, 3, 0, 2]))
print(s.maxProfit([5, 3, 3, 6, 9, 3, 10]))
print(s.maxProfit([5, 3, 3, 3, 3, 1, 10]))
print(s.maxProfit([5, 3]))
print(s.maxProfit([5]))
print(s.maxProfit([]))
