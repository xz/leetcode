class Solution:

    def countDigitOne(self, n: 'int') -> 'int':
        n_digits = len(str(n))
        for i in range(n_digits):
            b = 10**i
            a = n % (b * 10) // b
