def main():
    uf = {}

    def find(x):
        if x not in uf:
            uf[x] = (x, 0)

        if x != uf[x][0]:
            uf[x] = find(uf[x])

        return uf[x]

    input_list = [(0, 1), (1, 2), (3, 0), (4, 5), (5, 6), (7, 0), (8, 7), (7, 7)]

    for x, y in input_list:
        yy, yyr = find(y)
        xx, xxr = find(x)
        if yyr > xxr:
            uf[yy] = (xx, xxr)
            uf[xx] = (xx, xxr + 1)
        else:
            uf[xx] = (yy, yyr)

    for k in uf:
        find(k)

    print(uf)


if __name__ == '__main__':
    main()
