import re


def do_op(x, y, op):
    if op == '+':
        return x + y
    elif op == '-':
        return x - y
    elif op == '*':
        return x * y
    else:
        raise ValueError()


class Solution:

    def iter_all_bintree_configs(self, n_leaves, starting_idx):
        if n_leaves == 1:
            return [self.nums[starting_idx]]

        rets = []
        for n_leaves_in_left in range(1, n_leaves):
            left_rets = self.iter_all_bintree_configs(n_leaves_in_left, starting_idx)
            right_rets = self.iter_all_bintree_configs(n_leaves - n_leaves_in_left, starting_idx + n_leaves_in_left)

            for left_ret in left_rets:
                for right_ret in right_rets:
                    rets.append(do_op(left_ret, right_ret, self.ops[starting_idx + n_leaves_in_left - 1]))
        return rets

    def diffWaysToCompute(self, input):
        """
        :type input: str
        :rtype: List[int]
        """

        self.nums = list(map(int, re.split(r"[-+*]", input)))
        self.ops = re.split(r"[0-9]+", input)[1:-1]
        return sorted(self.iter_all_bintree_configs(len(self.nums), 0))


def main():
    s = Solution()
    print(s.diffWaysToCompute("2*3-4*5"))
    print(s.diffWaysToCompute("20*30-1+1"))


if __name__ == "__main__":
    main()
