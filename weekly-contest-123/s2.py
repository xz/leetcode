class Solution:

    def equationsPossible(self, equations):
        table = [[1 if i == j else 0 for j in range(26)] for i in range(26)]
        for eq in equations:
            v1 = ord(eq[0]) - ord('a')
            v2 = ord(eq[3]) - ord('a')
            if eq[1] == '=':
                for i in range(26):
                    if table[v1][i] * table[v2][i] == -1 or table[i][v1] * table[i][v2] == -1:
                        # print(f"assigning same to diff set: v1 = {v1}, v2 = {v2}")
                        # print('\n'.join([' '.join([str(x).rjust(2) for x in row]) for row in table]))
                        return False
                    else:
                        table[v2][i] = table[v1][i] = table[v1][i] | table[v2][i]
                        table[i][v2] = table[i][v1] = table[i][v1] | table[i][v2]
            else:
                for i in range(26):
                    if (table[v1][i] == 1 and table[v2][i] == 1) or (table[i][v1] == 1 and table[i][v2] == 1):
                        # print(f"assigning diff to same set: v1 = {v1}, v2 = {v2}")
                        # print('\n'.join([' '.join([str(x).rjust(2) for x in row]) for row in table]))
                        return False
                    else:
                        if table[v1][i] == 1 and table[v2][i] == 0:
                            table[v2][i] = -1
                        elif table[v1][i] == 0 and table[v2][i] == 1:
                            table[v1][i] = -1

                        if table[i][v1] == 1 and table[i][v2] == 0:
                            table[i][v2] = -1
                        elif table[i][v1] == 0 and table[i][v2] == 1:
                            table[i][v1] = -1

        # print('\n'.join([' '.join([str(x).rjust(2) for x in row]) for row in table]))
        return True


s = Solution()
print(s.equationsPossible(["a==b", "b!=c", "c==a"]))
