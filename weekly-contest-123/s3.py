from collections import deque


class Solution:

    def brokenCalc(self, X, Y):
        y = Y
        s = 0
        while y > X:
            if y % 2 == 0:
                y //= 2
            else:
                y += 1
            s += 1

        return X - y + s


s = Solution()
print(s.brokenCalc(2, 3))
print(s.brokenCalc(5, 8))
print(s.brokenCalc(3, 10))
print(s.brokenCalc(1, 1000000000))
