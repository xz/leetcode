class Solution:

    def addToArrayForm(self, A, K):
        return list(map(int, list(str(int(''.join(map(str, A))) + K))))


s = Solution()
print(s.addToArrayForm([9, 9, 9, 9, 9, 9, 9, 9, 9, 9], 1))
